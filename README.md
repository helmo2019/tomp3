## tomp3 - Lua program to convert music collections to MP3, making use of background processing
### Usage
`lua tomp3.lua` or `chmod +x tomp3.lua && ./tomp3.lua` to run. No CLI.

WARNING: Trying to convert large collections will crash right now, as there is
no limit to the amount of conversions running at the same time!!!

#### Shell integration
1. Obtain the `tomp3.lua` and `launch_tomp3.sh` files from this repository
2. Obtain the `libparallel.lua` file from
   [this repository](https://codeberg.org/helmo2019/lua-parallel-processes)
3. Place these files / (symbolic) links to them into a directory that's on
   your `$PATH`
4. Make `launch_tomp3.sh` executable (`chmod +x launch_tomp3.sh`). It is also
   recommended to rename it to something short, such as `tomp3`

#### Dependencies
This program does **not** require **any additional LUA modules**.
However, it **does** rely on the following programs / commands:
- **bash** for certain syntax
- **ffmpeg** for actually converting
- **kid3-cli** for retaining metadata on output files
- **ps** to check the status of started processes (`procps` package on debian, 
  should be preinstalled. Needs to support the **-p** option, which, as far as
  I know, is only not supported in the **BusyBox** version)
- **mkdir**, **rm** & **ls** (should be preinstalled on any *nix (GNU/Linux, Mac OS X) system)
  for creating directories and deleting files
- The **`io.popen()`** LUA function needs to be supported

I tested the program on my standard Debian 12 installation. I'm pretty sure it will **not work on Windows**.

### How does it work?
First, a list of tracks for conversion is compiled. The program searches **every folder in the working directory**
and will convert **.mp3**, **.flac**, and **.wav** files.

The resulting files will be output to a **`./MP3/`** directory. If this
directory already exists, you will be asked whether you want to clear it's
contents or cancel the program. **Conversions will only start if the output directory is empty.**

#### Background processing
Instead of running the `ffmpeg` commands one at a time, they are instead started as
**background processes**, meaning the LUA program can immediately move on to starting
the next command, instead of having to wait for each to finish. This way, the conversion
processes cann all run at the same time, and since this shouldn't be too CPU heavy, this
approach performs pretty well. All processes are registered in a *task list*, storing the
following information:
- PID (Process ID)
- ffmpeg command line
- kid3-cli command (for copying of metadata)

After all background ffmpeg processes are started, the program starts monitoring them.
The task list is iterated through continuously. If no process with the stored PID is found
or a new process has occupied that PID, resulting in it's command line and the command line
stored in the list no longer matching, the `kid3-cli` process is started in the background
and the ffmpeg process entry is removed from the list. The PIDs and command lines of all
kid3-cli commands are stored in a list as well.

After all ffmpeg processes have finished, the program now loops through the list of
kid3-cli processes, removing entries if their corresponding process has finished, until
the list is empty, meaning all kid3-cli processes are done.

This way, the user is only returned to the terminal once **all processes are finished**.

#### Hacky Methods
In the process of writing this program, I've frequently made use of very hacky approaches
when it comes to interacting with the system (e.g. checking the status of a process).

##### Running programs in the background
The procedure of this looks something like this:
- Append the ` < /dev/null > /dev/null 2> /dev/null & disown\necho $! > .pid\n` sequence to the command
   to the command.
- - `< /dev/null` means that the program receives standard input from /dev/null, 
    meaning no data is read at all. This is required, because otherwise, a key 
    press of the user may interrupt/pause the background process (at least it was 
    like this when I tested it in the terminal)
- - `> /dev/null` and `2> /dev/null` means that stdout and stderr are redirected
    to /dev/null, resulting in no output from the command being visible in the
    user's terminal
- - `& disown` runs the process in the background
- - `\necho $! > .pid\n` appends a new line (`\n`), followed by the
    `echo $! > .pid` command and another new line. This results in the
    PID of the last started process (so the one that was started in the background
    in the first line) being written to a `.pid` file in the current working
    directory. This way, the PID can be retreived later.
- Write the command with the appended sequence to the `.tmp` file
- Invoke `bash .tmp` file to run the commands in the .tmp text file (required, as
  sh, which LUA invokes when using the `os.execute` function, does not support
  `disown`)
- Read the `.pid` file into a variable

##### Checking whether a process is still alive
Here, I used the `ps` command like this: `ps ww -p PID -o args`, with
PID being the ID of the process that I want to check. The output of
this will look like this:  

    COMMAND  
    command arg1 arg2 arg3 ...

So, I just have to look at the second line and compare it with whatever
command line I have previously saved in my lists. If they match, I know
that the process I am looking for is still around!