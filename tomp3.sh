#!/bin/bash
out="MP3/"
real_out="$(realpath "${out}")"

# Handle existing out dir
if [[ ! -d ${out} ]]; then
    mkdir ${out}
else
    read -p "Output directory $(realpath ${out}) already exists. Clear? (y/*)" reply

    if [[ ${reply} == "y" ]]; then
        rm -rf ${out}/*
    else
        echo "Output directory remains. ERRORS MAY OCCUR!"
    fi
fi

echo -e "real_out is ${real_out}\n"
for dir in */
do
    real_dir="$(realpath "${dir}")"
    echo -e "dir is ${dir}\nreal_dir is ${real_dir}"
    
    if [[ ! "${dir}" == "Album Covers/" && ! "${dir}" == "MP3/" ]]; then
        real_aout="${real_out}/${dir}"
        echo "real_aout is ${real_aout}"
        mkdir "${real_aout}"
        
        for real_track in "${real_dir}"/*
        do
            track_fname="${real_track##"${real_dir}"}"
        
            ext="NONE"
            if [[ "${track_fname}" == *.flac ]]; then
                ext="flac"
            elif [[ "${track_fname}" == *.wav ]]; then
                ext="wav"
            elif [[ "${track_fname}" == *.mp3 ]]; then
                ext="mp3"
            fi
            
            echo "track_fname is ${track_fname}"
        
            if [[ ! "${ext}" == "NONE" ]]; then
                real_track_out="${real_aout}/${track_fname%%"${ext}"}mp3"
                echo "at track ${real_track} with name ${track_fname} with ext ${ext} (maybe) outputting to ${real_track_out}"

                if [[ "${ext}" == "flac" || "${ext}" == "wav" ]]; then
                    echo "converting"
                    ffmpeg -y -i "${real_track}" -strict experimental -acodec libmp3lame -ab 192k -ar 44100 -ac 2 -vn "${real_track_out}"
                    kid3-cli -c select "${real_track}" -c copy -c select "${real_track_out}" -c paste -c save
                elif [[ ext == "mp3" ]]; then
                    echo "copying"
                    cp "${real_track}" "${real_track_out}"
                fi
            
            fi
        
        done
    fi

    echo
done
