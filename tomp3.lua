#!/bin/lua
require "libparallel"

--- Utility
-- Catch the output of a command (modern version)
local function output_of(command)
    local handle = io.popen(command)
    local result = {}

    if(handle ~= nil) then
        local read;
        repeat
            read = handle:read("l")
            table.insert(result, read)
        until read == nil

        handle:close();
    end

    return result;
end

-- Shellscript x lua fuckury
local function dir_exists(path)
    os.execute("if [ -d "..path.." ] ; then echo 1 > .tmp ; else echo 0 > .tmp ; fi")

    local file = io.open(".tmp", "r");
    local result = 0;
    if (file ~= nil) then
        result = file:read("n")
        file:close();
        os.remove(".tmp")
    end

    return (result == 1)
end

-- Prompt YES/NO
local function prompt_yes_no(prompt)
    -- Set input/output sources
    io.output(io.stdout); io.input(io.stdin)
    -- Write prompt
    io.write(prompt.." (y/n)")

    -- Get and process response
    local response = io.read("l")
    return (response == "y" or response == "") -- 'y' or '' means yes, everything else means no
end

-- Prompt for number
local function prompt_int(prompt)
    io.output(io.stdout); io.input(io.stdin)

    io.write(prompt.." (int) ")
    local response, _ = io.read("n"), io.read("l")
    return response
end

-- Merge tables
local function merge_tables(storage, data_source)
    for _, entry in ipairs(data_source) do
        table.insert(storage, entry)
    end
end

-- Shell shortcuts
-- Get the real path of a file
local function real_path(file)
    return output_of("realpath \""..file.."\"")[1];
end

-- mkdir -p
local function mkdirs(path)
    os.execute("mkdir -p \""..path.."\"")
end

-- remove recursively
local function rmr(path)
    os.execute("rm -rf "..path)
end

-- list dir
local function ls(path)
    return output_of("ls -A1 "..path)
end

-- basename
local function basename(path)
    return output_of("basename "..path)[1]
end

-- remove suffix
local function remove_suffix(filename, options)
    local result, i = filename, 1
    repeat
        result = basename("\""..filename.."\" "..options[i])

        i = i+1
    until result ~= filename or i > #options

    if result == filename then
        return result, -1
    else
        return result, options[i-1]
    end
end

-- check if process is alive
-- https://unix.stackexchange.com/questions/163145/how-to-get-whole-command-line-from-a-process
-- https://www.baeldung.com/linux/ps-view-long-commands
local function is_process_alive(pid, command)
    local output = output_of("ps ww -p "..pid.." -o args")
    if output == {} then
        return false
    end

    return output[2] == command -- Second line (either nil or a command) should match the command
end

-- run in background. returns the PID of the newly started command
-- https://stackoverflow.com/questions/17621798/linux-process-in-background-stopped-in-jobs
-- https://unix.stackexchange.com/questions/163145/how-to-get-whole-command-line-from-a-process
local function execute_in_background(command)
    -- Run a process in the background with
    -- this hacky method
    -- (output command to file -> run file with bash)
    -- ... this is required because sh does not
    -- recognize the '& disown' part that makes the
    -- process run in the background
    io.output(".tmp")
    io.write(command.." < /dev/null > /dev/null 2> /dev/null & disown\necho $! > .pid\n")
    io.flush()
    io.output(io.stdout)

    os.execute("bash .tmp")

    io.input(".pid")
    return io.read("n")
end

-- This needs to be it's own function, otherwise
-- all hooks will run the same command
local kid3_processes, kid3_processes_completed = 0, 0
local function generate_kid3_hook(command, manager)
    return function ()
        manager:queue_add(command)
        kid3_processes_completed = kid3_processes_completed + 1
    end
end

--- Main Program
-- Set variables
local out_dir = real_path("./MP3/");

-- Get parameters
local max_ffmpeg, max_kid3 = prompt_int("Max ffmpeg processes?"), prompt_int("Max kid3-cli processes?")

-- Handle output directory
if (dir_exists(out_dir)) then
    if (prompt_yes_no("Output directory "..out_dir.." already exists. Clear?")) then
        rmr(out_dir.."/*")
    else
        print("Please make sure the output directory '"..out_dir.."' is empty or nonexistent before proceeding.")
        os.exit(1)
    end
else
    mkdirs(out_dir)
end

-- Start processing
local manager_ffmpeg = parallel.process_manager(max_ffmpeg)
local manager_kid3 = parallel.process_manager(max_kid3)

local album_path, album_out, tracks_count, tracks_list, track_name, track_out, extension, command_ffmpeg, command_kid3
local extensions = {".mp3", ".flac", ".wav"} -- Searching order
local dirs = ls("-d */")

for _, dir in ipairs(dirs) do
    if dir ~= "MP3/" then
        album_path = real_path(dir)
        print("Queuing conversions for: ", album_path)
        tracks_count = 0
        tracks_list = {}
        album_out = real_path(out_dir.."/"..dir)

        -- Search for convertable files
        for _, ext in ipairs(extensions) do
            merge_tables(tracks_list, ls("\""..album_path.."/\"*"..ext.." 2> /dev/null"))
        end
        tracks_count = #tracks_list

        -- Queue conversions
        if (tracks_count ~= 0) then
            -- Only create output dir if tracks were found
            mkdirs(album_out)

            for _, track in ipairs(tracks_list) do
                track_name, extension = remove_suffix(basename("\""..track.."\""), extensions)
                track_out = real_path("MP3/"..dir.."/"..track_name..".mp3")
                
                -- Convert
                if extension == ".mp3" then
                    -- We can simply copy
                    os.execute("cp \""..track.."\" \""..track_out.."\"")
                else
                    -- FFMPEG time
                    command_ffmpeg = "ffmpeg -i \""..track.."\" -y -strict experimental -acodec libmp3lame -ab 192k -ar 44100 -ac 2 -vn \""..track_out.."\""
                    command_kid3 = "kid3-cli -c copy \""..track.."\" -c paste \""..track_out.."\" -c save"

                    manager_ffmpeg:queue_add(command_ffmpeg, generate_kid3_hook(command_kid3, manager_kid3))
                    kid3_processes = kid3_processes + 1
                end
            end
        end
    end
end

-- Progress
local progress = {
    ffmpeg = {
        total = #manager_ffmpeg.queue,
        one_percent = #manager_ffmpeg.queue / 100,
        last = 0.0,
        current = 0.0
    },
    kid3 = {
        total = kid3_processes,
        one_percent = kid3_processes / 100,
        last = 0.0,
        current = 0.0
    }
}
local function print_progress()
    print(string.format("%10s  %-10s",
        string.format("%3.1f%%", progress.ffmpeg.current),
        string.format("%3.1f%%", progress.kid3.current)
    ))
end

print("\nBeginning conversion now. This may take a while!\nProgress:")
print("conversion  metadata")

repeat
    manager_ffmpeg:queue_next()
    manager_kid3:queue_next()

    -- Current becomes last
    progress.ffmpeg.last = progress.ffmpeg.current
    progress.kid3.last = progress.kid3.current

    -- Calculate new values
    progress.ffmpeg.current = 1.0 * ( (progress.ffmpeg.total - (#manager_ffmpeg.queue + manager_ffmpeg.running_tasks)) / progress.ffmpeg.one_percent )
    progress.kid3.current = 1.0 * ( kid3_processes_completed / progress.kid3.one_percent )

    -- Print if the values changed
    if (progress.ffmpeg.current ~= progress.ffmpeg.last) or (progress.kid3.current ~= progress.kid3.last) then
        print_progress()
    end


until manager_ffmpeg:all_finished() and manager_kid3:all_finished()

--[[
    (Note I made after spending 1h on a pointless implementation)

    ACTUALLY: This method is BS. This way, the
    program only waits for the LAST process to
    finish.
    What we actually have to do is MONITOR ALL
    PROCESSES until they're all gone.
    so...

    TODO:
    - Log PIDs of all started processes (easy, just add 'echo $! > .pid' to commands + read the .pid file) - done
    - Find simply way of checking if a process
      is alive / checking the status of a process - done
    - Abstract the process to functions so that
      it can be repeated for the ffmpeg and for the
      kid3-cli process runs - nah
    
    - ALTERNATIVE: Make a map 'ffmpeg pid -> kid3-cli command',
      monitor the status of all mapped processes. If
      a process disappears, run the kid3 command in the
      background and remove the map entry.
      DOWNSIDES: How do we know if all processes are actually
      finished? Returning the user to the terminal WHILE
      BACKGROUND PROCESSES ARE STILL RUNNING would be
      stupid and unsafe. So running the ffmpeg and kid3-cli
      processes sequentially might be better. This alternative
      would benefit from parallel processing, though.
      TODO: Prototype this and see if it yields time saves
      ^_ this is what I ended up doing: start all ffmpeg commands,
         then monitor them and start the kid3-cli commands if a
         ffmpeg process finished, then wait until all kid3-cli
         processes are finished. in real time. no delays.
]]

-- Remove temporary files
os.remove(".tmp")
os.remove(".pid")